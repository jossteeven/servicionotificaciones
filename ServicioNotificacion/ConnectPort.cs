﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
namespace ServicioNotificacion
{
    class ConnectPort
    {
        // Create the serial port with basic settings 
        private SerialPort port;
        private String numberPort;
        private int Bandwith;
        public string respond;
        private LinkedList<Byte> Comando_Respuesta = new LinkedList<Byte>();


        public ConnectPort(string numberPort, int bandwith)
        {
            this.numberPort = numberPort;
            Bandwith = bandwith;
            this.port = new SerialPort(this.numberPort,this.Bandwith, Parity.None, 8, StopBits.One);
            this.port.Handshake = Handshake.XOnXOff;
            this.port.Open();
            this.port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }

        public void sendCommand(String command, String mensaje, String numero)
        {
            Console.WriteLine("El comando enviado es: "+command);
            this.port.Write(command);
            Thread.Sleep(100);

        }
        public void sendSMS(String mensaje, String numero)
        {
            
            this.port.Write("AT+CMGF=1\r");
            Thread.Sleep(100);
            this.port.Write("AT+CSMP=17,167,2,0\r");
            Thread.Sleep(100);
            this.port.Write("AT+CSCS=\"GSM\"\r");
            Thread.Sleep(100);
            this.port.Write("AT+CMGS=" + (char)34 + numero + (char)34 + "\r");
            Thread.Sleep(100);
            this.port.Write(mensaje + "\x1A");
            Thread.Sleep(100);
            this.port.Write("ATE1\r");
            Thread.Sleep(100);

        }
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            respond = this.port.ReadExisting();
            
        }


        

    }
}
